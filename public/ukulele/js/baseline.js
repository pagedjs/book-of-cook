class baselineMover extends Paged.Handler {
  constructor(chunker, polisher, caller) {
    super(chunker, polisher, caller);
  }

  renderNode(node, sourceNode) {
    // if (
    //   node.nodeType == 1 &&
    //   node.tagName == "P" &&
    //   node.parentElement.tagName == "SECTION" &&
    //   node.parentElement.firstElementChild == node
    // ) {
    //   startBaseline(node, 16);
    // }
    if (
      node.nodeType == 1 &&
      node.tagName == "P" &&
      node.previousElementSibling &&
      node.previousElementSibling.style.display != "none"
    ) {
      if (
        node.previousElementSibling.tagName === "H2" ||
        node.previousElementSibling.tagName === "H3" ||
        node.previousElementSibling.tagName === "OL" ||
        node.previousElementSibling.tagName === "UL" ||
        node.previousElementSibling.classList.contains("bq") ||
        node.previousElementSibling.classList.contains("realWorld") ||
        node.previousElementSibling.classList.contains("figureReal") ||
        node.previousElementSibling.classList.contains("personal_finance") ||
        node.previousElementSibling.classList.contains("table")
      ) {
        startBaseline(node, 16);
      }
    }
  }
}

Paged.registerHandlers(baselineMover);

function startBaseline(element, baseline) {
  // snap element after specific element on the baseline grid.
  //   baseline = 16;

  if (element) {
    const elementOffset = element.offsetTop;
    const elementline = Math.floor(elementOffset / baseline);

    if (elementline != baseline) {
      const nextPline = (elementline + 1) * baseline;

      if (!(nextPline - elementOffset == baseline)) {
        element.style.paddingTop = `${nextPline - elementOffset}px`;
      }
    }
  }
}
