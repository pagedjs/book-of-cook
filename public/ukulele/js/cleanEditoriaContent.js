class cleanContent extends Paged.Handler {
  constructor(chunker, polisher, caller) {
    super(chunker, polisher, caller);
  }
  beforeParsed(content) {
    // remove toc

    if (content.querySelector("section.toc")) {
      content.querySelector("section.toc").remove();
    }
    // remove running heads
    if (content.querySelector(".running-left")) {
      content.querySelector(".running-left").remove();
      content.querySelector(".running-right").remove();
    }
    // remove empty em
    content.querySelectorAll("em").forEach((em) => {
      if (em.querySelector("em")) {
      }
      if (em.textContent == "" || em.textContent == " ") {
        em.remove();
      }
    });
  }
}

Paged.registerHandlers(cleanContent);

class setUpContent extends Paged.Handler {
  constructor(chunker, polisher, caller) {
    super(chunker, polisher, caller);

    this.homeIcon = `<svg width="143" height="132" viewBox="0 0 143 132" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M77.1733 0.850006L95.3526 20.5275V6.07797L115.966 6.07799V42.8402L142.664 71.7376H126.586V82.5V132L82.6398 132V93.227H60.9124V132L16.9665 132V71.7376H0.888494L77.1733 0.850006Z" fill="#C4C4C4" /> </svg>`;
  }
  beforeParsed(content) {
    createSpanInTitle(content, ".homeIcon_incentive");

    toSpanInNextElement(content, `h4`, ["title-h3"]);

    // create homeIcon

    content.querySelectorAll(".homeIcon_incentive").forEach((homeIcon) => {
      homeIcon.classList.remove("homeIcon_incentive");
      homeIcon.classList.add("homeIcon");
    });

    // toSpanInNextElement(content, `.homeIcon_incentive`, ["homeIcon"]);

    addIcon(content, `.homeIcon`, this.homeIcon);

    // createblockintro
    createBlock(content, "div", "intro", [
      ".chapter_intro_title",
      ".chapter_intro_text",
    ]);

    createBlock(content, "div", "bq", [".big_questions"]);

    createBlock(content, "div", "personal_finance", [
      ".personal_finance_source",
      ".personal_finance_title",
      ".personal_finance_text",
    ]);
    createBlock(content, "div", "realWorld", [
      ".realworld_title",
      ".realworld_text",
      ".realworld_source",
    ]);

    createBlock(content, "div", "practice", [
      ".pratice_title",
      ".practice_source",
      ".practice_text",
      ".practice_answer",
      ".practice_question",
    ]);

    createBlock(content, "div", "table", [".tableTitle", "table"]);

    createBlock(content, "div", "concepts", [".concept_title"]);

    content.querySelectorAll(".concept_link").forEach((link) => link.remove());

    changeTag(content, "h2", ".chapter_intro_title");

    createBlock(content, "div", "lifeEconomics", [
      ".lifeEconomics_title",
      ".lifeEconomics_text",
    ]);
    createBlock(content, "div", "bigTest", [
      ".bigTest_question",
      ".bigTest_answer",
    ]);
    createBlock(content, "div", "studyProblem", [
      ".studyProblem_title",
      ".studyProblem_question",
      ".studyProblem_list",
    ]);
    createBlock(content, "div", "studySolution", [
      ".solved_title",
      ".studyProblem_solution",
    ]);

    createBlock(content, "div", "figureReal", [".figureTitle"]);
    createBlock(content, "div", "questionsReview", [
      ".question_for_review_title",
      ".question_for_review",
    ]);

    createCover(content);

    chapterNumberEffect(content);

    changeTag(content, "h2", ".chapter_intro_title");
    changeTag(content, "h3", ".personal_finance_title");
    changeTag(content, "h3", ".realworld_title");
    changeTag(content, "h3", ".pratice_title");
    changeTag(content, "h3", ".lifeEconomics_title");

    changeTag(content, "h3", ".tableTitle");
    createSpanInTitle(content, ".tableTitle");

    createSpanInTitle(content, ".big_questions");

    createTitleToAside(content, `h3`, `Personal Finance`, `.personal_finance`);
    createTitleToAside(
      content,
      `h3`,
      `Answering <span class='small'>the</span> big questions`,
      `.bigTest`
    );
    createTitleToAside(content, `h3`, `Practice what you know`, `.practice`);
    createTitleToAside(
      content,
      `h3`,
      `<span class="eco">Economics</span> <span class="for">for</span> <span class="life">life</span>`,
      `.lifeEconomics`
    );

    createTitleToAside(
      content,
      `h3`,
      `Economics in the real world`,
      `.realWorld`
    );

    pushImageToTopBlock(content, `.personal_finance`);
    pushImageToTopBlock(content, `.realWorld`);
    pushImageToTopBlock(content, `.lifeEconomics`);
    pushImageToTopBlock(content, `.practice`);

    homeIconAddMargins(content);

    // content
    //   .querySelectorAll(".personal_finance")
    //   .forEach((pf) => pf.classList.add("page-float-next-top"));
    // console.log(content.querySelectorAll(".personal_finance"));
    fixHomeIconInList(content);
    fixList(content);

    changeTag(content, "h3", ".concept_title");
    changeTag(content, "h3", ".question_for_review_title");
    changeTag(content, "h3", ".studyProblem_title");
    changeTag(content, "h3", ".solved_title");

    mergeFigures(content);

    addIDtoEachElement(content);
    makeEnd(content);
  }
}

Paged.registerHandlers(setUpContent);

function changeTag(content, tagEl, element) {
  content.querySelectorAll(element).forEach((e) => {
    d = document.createElement(tagEl);
    d.classList = e.classList;
    d.id = e.id;
    d.innerHTML = e.innerHTML;
    e.parentNode.replaceChild(d, e);
  });
}

// create the cover
function createCover(content) {
  if (content.querySelector(".intro figure img ")) {
    let coverWrapper = document.createElement("figure");
    coverWrapper.classList.add("cover");

    coverWrapper.insertAdjacentElement(
      "afterbegin",
      content.querySelector(".intro figure img ")
    );
    let figcaption = content.querySelector(".intro figure figcaption");
    figcaption.classList.add("introcaption");
    content
      .querySelector(".intro")
      .insertAdjacentElement("beforebegin", figcaption);
    content
      .querySelector("header")
      .insertAdjacentElement("beforebegin", coverWrapper);
    content.querySelector(".intro").querySelector("figure").remove();
  }
}

// add Chapter num attribute to the chapter number to reuse it through css
function chapterNumberEffect(content) {
  const chapterNum = content.querySelector(".chapter-number");
  if (chapterNum) {
    chapterNum.dataset.chapNum = chapterNum.textContent;
    const chapterWord = document.createElement("span");
    chapterWord.classList.add("chapterWord");
    chapterWord.innerHTML = "chapter";

    chapterNum.insertAdjacentElement("afterbegin", chapterWord);
  }
}

// WorkOnlyWithAllElement on the page. Need one with only the following elements: check if the class following contains the one we need.

// getFirstElement,

function createBlock(content, elementTag, elementClassName, elements) {
  // create wrapper block

  let allFig = content.querySelectorAll(
    "figure, ul, ol, blockquote, .homeIcon"
  );
  allFig.forEach((fig) => {
    if (
      testClass(fig.previousElementSibling, elements) ||
      fig.previousElementSibling.classList.contains(
        `${elementClassName}-wrapper`
      )
    ) {
      fig.classList.add(`${elementClassName}-wrapper`);
    }
  });

  // move stiff in the previous box

  // set an anchor before each first element
  elements.forEach((ct) => {
    content.querySelectorAll(ct).forEach((elementWithClass, index) => {
      if (
        !testClass(elementWithClass.previousElementSibling, elements) &&
        !elementWithClass.previousElementSibling.classList.contains(
          `${elementClassName}-wrapper`
        )
      ) {
        let block = document.createElement(elementTag);
        block.classList.add(elementClassName);
        block.classList.add("blockWrapper");
        elementWithClass.insertAdjacentElement("beforebegin", block);
      }
    });
  });

  // add Class To all image

  // Add Class To All Element
  elements.forEach((ct) => {
    content.querySelectorAll(ct).forEach((el) => {
      el.classList.add(`${elementClassName}-wrapper`);
      let next = el.nextElementSibling;
      if (
        next &&
        (next.tagName == "BLOCKQUOTE" ||
          next.tagName == "FIGURE" ||
          next.tagName == "OL" ||
          next.tagName == "UL")
      ) {
        el.nextElementSibling.classList.add(`${elementClassName}-wrapper`);
      }
    });
  });

  content
    .querySelectorAll(`.${elementClassName}-wrapper`)
    .forEach((wrappedElement) => {
      if (wrappedElement.tagName === "figure") {
      }
      wrappedElement.previousElementSibling.insertAdjacentElement(
        "beforeend",
        wrappedElement
      );
    });
}

function testClass(el, array) {
  // remove # and . to the element lists
  let arrayUpdate = [];

  array.forEach((cn) => {
    cn = cn.replace("#", "");
    cn = cn.replace(".", "");
    arrayUpdate.push(cn);
  });
  // creates test case

  let classes = "\\b(" + arrayUpdate.join("|") + ")\\b",
    regex = new RegExp(classes, "i");
  if (regex.test(el.classList.value)) {
    return true;
  } else {
    return false;
  }
}

// move the element to the beginning of the next element as a span
function toSpanInNextElement(content, element, classNames) {
  if (element.tag != "SPAN") {
    let list = content.querySelectorAll(element);
    list.forEach((item) => {
      let createdSpan = document.createElement("span");
      classNames.forEach((classNameSingle) => {
        createdSpan.classList.add(classNameSingle);
        item.nextElementSibling.classList.add(`wrapper-${classNames}`);
      });
      createdSpan.innerHTML = item.innerHTML;
      item.nextElementSibling.insertAdjacentElement("afterbegin", createdSpan);
      item.remove();
    });
  }
}

function createSpanInTitle(content, lookedElement) {
  content.querySelectorAll(lookedElement).forEach((el) => {
    let createdSpan = document.createElement("span");
    createdSpan.innerHTML = el.innerHTML;
    el.innerHTML = createdSpan.outerHTML;
  });
}

function createTitleToAside(content, tag, title, hosts) {
  content.querySelectorAll(hosts).forEach((host) => {
    let titleBlock = document.createElement(tag);
    titleBlock.classList.add(`addedTitle`);
    titleBlock.innerHTML = `<span>${title}</span>`;
    host.insertAdjacentElement("afterbegin", titleBlock);
  });
}

function pushImageToTopBlock(content, guest) {
  let host = content.querySelector(guest);
  if (host) {
    host.querySelectorAll("figure").forEach((blockImage) => {
      host.insertAdjacentElement("afterbegin", blockImage);
    });
  }
}

function addIcon(content, host, icon) {
  content.querySelectorAll(host).forEach((hiblock) => {
    hiblock.insertAdjacentHTML("afterbegin", icon);
  });
}

function homeIconAddMargins(content) {
  content.querySelectorAll(".homeIcon").forEach((homeIcon) => {
    if (homeIcon.previousElementSibling.classList.contains("homeIcon")) {
      homeIcon.classList.add("next");
    }
  });
}

function mergeFigures(content) {
  content.querySelectorAll("figure").forEach((fig) => {
    if (
      fig.previousElementSibling &&
      fig.previousElementSibling.tagName == "FIGURE"
    ) {
      fig.previousElementSibling.classList.add("doubleFig");
      fig.previousElementSibling.insertAdjacentHTML("beforeend", fig.innerHTML);
      fig.remove();
    }
  });
  /* margin-right: 1.5ch; */
}

function addIDtoEachElement(content) {
  let tags = ["figure", "figcaption", "img", "ol", "ul", "li", "p", "img", "table", "h1", "h2", "h3", "div", "aside"];
  tags.forEach( tag => {
    content.querySelectorAll(tag).forEach((el, index) => {
      if (!el.id) {
        el.id = `el-${el.tagName.toLowerCase()}-${index}`;
      }
    });
  })
}

function makeEnd(content) {
  let chapterProblem = document.createElement("div");
  chapterProblem.classList.add("chapterProblems");
  let title = document.createElement("h2");
  title.innerHTML = "<span>Chapter problems</span>";
  chapterProblem.appendChild(title);
  content
    .querySelectorAll(
      ".concepts, .questionsReview , .studyProblem, .studySolution"
    )
    .forEach((bit) => {
      chapterProblem.insertAdjacentElement("beforeend", bit);

      content.appendChild(chapterProblem);
    });
}

function fixHomeIconInList(content) {
  content.querySelectorAll('.homeIcon').forEach(homeIcon => {

    if(homeIcon.previousElementSibling && (homeIcon.previousElementSibling.tagName == 'OL' || homeIcon.previousElementSibling.tagName == 'OL')) {
      homeIcon.previousElementSibling.insertAdjacentElement('beforeend', homeIcon);
    }
  })
}

function fixList(content) {
  content.querySelectorAll("ol").forEach((list) => {
    if (
      list.previousElementSibling &&
      list.previousElementSibling.tagName === "OL"
    ) {
      list.innerHTML = list.previousElementSibling.innerHTML + list.innerHTML;
      list.previousElementSibling.parentNode.removeChild(
        list.previousElementSibling
        
      );
    }

    // list.remove();
  });
}
