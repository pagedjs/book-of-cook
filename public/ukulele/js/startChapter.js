class moveIntroCaption extends Paged.Handler {
  constructor(chunker, polisher, caller) {
    super(chunker, polisher, caller);
    this.bottom;
  }

  renderNode(nextClone, node) {
    // If you find a float page element, move it in the array,
    if (node.nodeType == 1 && node.classList.contains(`introcaption`)) {
      let elementHeight = nextClone.getBoundingClientRect().height;

      let page = document.querySelector(".pagedjs_pages").lastElementChild;

      let pageHeight = page
        .querySelector(".pagedjs_page_content")
        .getBoundingClientRect().height;
      // console.log(pageHeight, elementHeight)
      nextClone.style.marginTop = `${pageHeight - elementHeight - 48}px`;
      this.bottom =
        nextClone.getBoundingClientRect().bottom -
        nextClone.getBoundingClientRect().top +
        elementHeight / 2;
      // console.log(this.bottom)
    }
  }

  afterPageLayout(page) {
    // add all the yellow borders decoration
    if (page.querySelector(".cover")) {
      addBottomBlock(page);
    }
    if (page.querySelector(".introcaption")) {
      addBottomBlock(page);

      // page.querySelector(".introcaption").classList.add("intromoved");
      let rightBorder = document.createElement("span");
      rightBorder.classList.add("borderRight");

      page
        .querySelector(".pagedjs_sheet")
        .insertAdjacentElement("afterbegin", rightBorder);
      let middleBorder = document.createElement("span");
      middleBorder.classList.add("borderMiddle");
      middleBorder.style.bottom = `${this.bottom}px`;

      page
        .querySelector(".pagedjs_sheet")
        .insertAdjacentElement("afterbegin", middleBorder);

      if (page.querySelector(".cover")) {
        addBottomBlock(page);
      }
    }
  }
}
Paged.registerHandlers(moveIntroCaption);

function addBottomBlock(page) {
  let bottomBlock = document.createElement("span");
  bottomBlock.classList.add("bottom-block");
  page
    .querySelector(".pagedjs_sheet")
    .insertAdjacentElement("afterbegin", bottomBlock);
}
