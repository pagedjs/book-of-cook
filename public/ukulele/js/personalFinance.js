class moveToNextPage extends Paged.Handler {
  constructor(chunker, polisher, caller) {
    super(chunker, polisher, caller);
    this.personalFinance = new Set();
  }

  renderNode(node, sourceNode) {
    if (node.nodeType == 1 && node.classList.contains("personal_finance")) {
      // console.log(node);
      // add the term to the set
      this.personalFinance.add(node);
      // node.remove()
      node.style.display = 'none';
      // remove the term form the element
      // node.remove();
    }
  }

  beforePageLayout(page) {

    if (this.personalFinance.size) {
      for (let item of this.personalFinance) {
        let content = document.createElement('article');
        content.classList.add('content');
        content.insertAdjacentElement('afterbegin', item);
        item.style.display = 'block';
        page.element.querySelector('.pagedjs_page_content').insertAdjacentElement('afterbegin', content);
        this.personalFinance.delete(item);
    }
  }
  }
}
//   let block = document.createElement("aside");
//   block.classList.add("key-terms-collection");

//   // calculate position bottom of the block
//   let finalBottom = pageElement
//     .querySelector(".pagedjs_page_content")
//     .querySelector("section")
//     .lastElementChild.getBoundingClientRect().bottom;

//   let pageBottom = pageElement
//     .querySelector(".pagedjs_page_content")
//     .getBoundingClientRect().bottom;

//   let blockBottom = pageBottom - finalBottom;

//   for (let item of this.personalFinance) {
//     block.insertAdjacentElement("beforeend", item);
//     this.personalFinance.delete(item);
//   }
//   pageElement
//     .querySelector(".pagedjs_page_content")
//     .insertAdjacentElement("afterbegin", block);

//   block.style.bottom = `${blockBottom}px`;
// }

// Paged.registerHandlers(moveToNextPage);
