class hyphenopoly extends Paged.Handler {
  constructor(chunker, polisher, caller) {
    super(chunker, polisher, caller);
  }
  beforeParsed(content) {

    // add lang fr to all sections
    content.querySelectorAll("section").forEach(section => {
      section.setAttribute("lang", "fr")
    })
  
    Object.keys(Hyphenopoly.setup.selectors).forEach(sel => {
      content.querySelectorAll(sel).forEach(elem => {
        Hyphenopoly.hyphenators["HTML"].then((hyn) => {
          hyn(elem, sel);
        });
      });
    });
  }
}

Paged.registerHandlers(hyphenopoly);
