#!/Users/julienbidoret/Code/virtualenvs/wildproject/bin/python
#-*- coding: utf-8 -*-

# imports
import random
import numpy

steps_nb = random.randint(40, 90)
steps = numpy.arange(1, 1171)
numpy.random.shuffle(steps)
steps=steps[:steps_nb].tolist()
steps.sort()

print(steps)

# exit()

current_wdth = 400
target_wdth = random.randint(0, 800)
current_wght = 400
target_wght = random.randint(400, 500)



current_step = 0
next_step = steps[1]

augment_wdth = round( (target_wdth - current_wdth) / (next_step - current_step) )
augment_wght = round( (target_wght - current_wght) / (next_step - current_step) )

with open('vars.css', 'w') as f:
  step = 0
  
  for i in range(1171):
    step = step + 1      
    if( len(steps) and step > steps[0]):
      current_step = steps.pop(0)
      try:
        next_step = steps[0]
      except:
        next_step = 1171
      target_wdth = random.randint(0, 800)
      target_wght = random.randint(400, 500)

      augment_wdth = round((target_wdth - current_wdth) / (next_step - current_step))
      augment_wght = round((target_wght - current_wght) / (next_step - current_step))

      print("• étape {}".format(current_step))
      print('     width going from {} to {} in {} steps ({} -> {}), increasing by {}'.format(current_wdth, target_wdth, (next_step - current_step),  current_step, next_step, augment_wdth ))
      print('     weight going from {} to {} in {} steps ({} -> {}), increasing by {}'.format(current_wght, target_wght, (next_step - current_step),  current_step, next_step, augment_wght ))

    
    current_wdth = round(current_wdth + augment_wdth)
    current_wght = round(current_wght + augment_wght)
    
    f.write(u"[data-id='span-{}'] {{ font-variation-settings: 'wdth' {}, 'wght' {} }}".format(i, current_wdth, current_wght))
  
  

  for i in range(84):
    wdth = random.randint(40, 60)
    float = "left" if random.randint(1, 2) == 1 else "right"
    margin = "margin-left: -40px;" if float == "left" else "margin-right: -40px"
    f.write(u"[data-id='image-{}'] {{ width : {}%; float: {}; {} }}".format(i, wdth, float, margin))      


