// lets you manualy add classes to some pages elements
// to simulate page floats.
// works only for elements that are not across two pages

const classElemFullPage = "imgFullPage"; // ← class of full page images
const classElemFullSpread = "imgFullSpread"; // ← class of full page images

class fullPage extends Paged.Handler {
  constructor(chunker, polisher, caller) {
    super(chunker, polisher, caller);
    this.floatFullPage = [];
    this.floatFullSpread = [];
    this.floatFullSpreadElements = new Set;
    this.floatFullElements = new Set;
    this.cloneset = false;
    this.clonestyle = `<style data-pagedjs-inserted-styles="true">.cloned {
    position: absolute;
    inset: 0;
    margin: 0;
    padding: 0;
    height: 100%;
    width: 100%;
    object-fit: cover;
}</style>`;
    this.pauseToken;
  }



  // this script will run on every css declaration 

  onDeclaration(declaration, dItem, dList, rule) {
    // if the declaration contains --page-float and taht page 
    if (declaration.property == "--page-float") {
      // if the value of that declaration is 'full-page'
      if (declaration.value.value.includes("full-page")) {
        // we use css tree to generate the list of selector
        let sel = csstree.generate(rule.ruleNode.prelude);
        // we make sure that any data-id is replaced by an id to be reused by pagedjs
        sel = sel.replace('[data-id="', "#");
        sel = sel.replace('"]', "");
        // we add the selector to the list and keep that in a variable shared by all the hooks.
        sel.split(',').forEach(el => this.floatFullPage.push(el));
      }
      // here we add the clonestyle css to the document (but we check first that it’s hasn’t been done yet to add it only once)
      if (this.cloneset == false) {
        document.head.insertAdjacentHTML('beforeend', this.clonestyle);
        this.cloneset = true;
      }

    }
  }

  afterParsed(content) {
    // when all the content is parsed, check for all the selectors in this.floatFullPage, and add a class (with the classElemFullPage variable set in the top of the file)
    if (this.floatFullPage) {
      this.floatFullPage.forEach((elNBlist) => {
        content.querySelectorAll(elNBlist).forEach((el) => {
          el.classList.add(classElemFullPage);
        });
      });
    }
  }


  // here comes the magic.
  // renderNode runs everytime pagedjs want to render an element
  // When we render the node (when the element will be added to the page)
  
  renderNode(node, sourcenode) {
    // if the node is of type element
    if (node.nodeType == 1) {
      // if the node has the class  
      if (node.classList.contains(classElemFullPage)) {
        // clone the node
        let clone = node.cloneNode(true);
        // remove the id of the clone (to be valid html)
        clone.id = '';
        // add the cloned class to the clone
        clone.classList = "cloned"
        
        // add the clone to the list of the element that will be push on their own page
        this.floatFullElements.add(clone);

        // add a border to the element to be cloned (to debug)
        node.style.border = '1px solid magenta';
        // hide it. We don’t remove it from the dom, otherwise pagedjs will be lost when checking the source and the note
        node.style.display = "none";
        // hide the sourcenode, we don’t need it anymore after all. (this is may be problematic one day, but i’ll be far when that happen )
        sourcenode.style.display = "none";

      }
    }
  }

  // this will happen before the layout of each and every page

  beforePageLayout(page) {
    // if there is an element in this.floatFullElements
    if (this.floatFullElements && this.floatFullElements.size >= 1) {

      // add the pullpage class to the page
      page.element.classList.add('fullpaged')
      // insert the first element of the set (the ... spread help us find the first element)
      page.element.querySelector('.pagedjs_sheet').insertAdjacentElement('afterbegin', [...this.floatFullElements][0]);
      //then delete the element from the this.floatFullElements][0]
      this.floatFullElements.delete([...this.floatFullElements][0]);


    }
  }


// this will happen after every page is finished (yeah!)

  afterPageLayout(pageElement, page, breakToken) {

    // if the image class contain full page
    if (pageElement.classList.contains('fullpaged')) {
      // hide everything in the margin
      pageElement.querySelectorAll(".pagedjs_margin-content")
        .forEach((marginContent) => marginContent.innerHTML = "");
        //remove the content from the page
      pageElement.querySelector('.pagedjs_page_content').innerHTML = '';
      // keep the breakk token in a variable
      breakToken = this.pauseToken;
      //and share it to the rest of the script
      return breakToken;
    }

  }

  // change the starting point of the page
  onBreakToken(breakToken, overflow, rendered) {


    // if there is something in the this.floatFullElements
    if (this.floatFullElements.size >= 1) {

      // save the break token object
      this.pauseToken = { ...breakToken };
      // add 1 to the breaktoken offset, so pagedjs get the feeling that the break token are differnt
      breakToken.offset = breakToken.offset + 1;
      
      //send the fake break token (that will be only used by the infinite loop protection)
      return (breakToken);
    }

    // if pauseToken exist
    else if (this.pauseToken != undefined) {
      
      // console.log(`breakToken`, breakToken);
      // return (breakToken);

      // reuse the value of the pause token for the newBreaktoken
      breakToken.node = this.pauseToken.node;
      breakToken.offset = this.pauseToken.offset;

      // reset the pauseToken
      this.pauseToken = undefined

      // send back the fake token.
      return breakToken;

    }
  }
}

Paged.registerHandlers(fullPage);
