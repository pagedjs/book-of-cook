
class revolution extends Paged.Handler {
  constructor(chunker, polisher, caller) {
    super(chunker, polisher, caller);
  }

  beforeParsed(content) {

    // injecte des images aléatoirement au fil du texte
    var ps = content.querySelectorAll('.text-section p');
    var images = ["p7pjreACJF1qgl6hw_1280-gray.jpg", "p7pclpweXy1qgl6hw_1280-gray.jpg", "p7pdi4lBAd1qgl6hw_1280-gray.jpg", "p7pj6zro4H1qgl6hw_1280-gray.jpg", "p7pjek6zVb1qgl6hw_1280-gray.jpg", "p7pji49q521qgl6hw_1280-gray.jpg", "p7pjl3vTLX1qgl6hw_1280-gray.jpg", "p7pjsmHaPy1qgl6hw_1280-gray.jpg", "p7pjwhBSU21qgl6hw_1280-gray.jpg", "p7pk03QPGR1qgl6hw_1280-gray.jpg", "p7pk5k2fvv1qgl6hw_1280-gray.jpg", "p7pk6jxwlT1qgl6hw_1280-gray.jpg", "p7pksp9jvf1qgl6hw_1280-gray.jpg", "p7pktxWiUa1qgl6hw_1280-gray.jpg", "p7pkw60FyS1qgl6hw_1280-gray.jpg", "p7pkyvCcew1qgl6hw_1280-gray.jpg", "p7plgtr0Y81qgl6hw_1280-gray.jpg", "p7plimPx6K1qgl6hw_1280-gray.jpg", "p7pljmHlWI1qgl6hw_1280-gray.jpg", "p7plkyhLtU1qgl6hw_1280-gray.jpg", "p7pm0bL7at1qgl6hw_1280-gray.jpg", "p7pm8vDGrk1qgl6hw_1280-gray.jpg", "p7pm34bhkP1qgl6hw_1280-gray.jpg", "p7pm661Jgw1qgl6hw_1280-gray.jpg", "p7pmxfudEn1qgl6hw_1280-gray.jpg", "p7pmzu3wze1qgl6hw_1280-gray.jpg", "p7pn2xMSm41qgl6hw_1280-gray.jpg", "p7pn13h42Q1qgl6hw_1280-gray.jpg", "p7pnc8eS7y1qgl6hw_1280-gray.jpg", "p7pnelAMcn1qgl6hw_1280-gray.jpg", "p7pnhwXTZM1qgl6hw_1280-gray.jpg", "p7pnj1auKO1qgl6hw_1280-gray.jpg", "p7pnn3COXr1qgl6hw_1280-gray.jpg", "p7pnozf0L71qgl6hw_1280-gray.jpg", "p7pnphhq3u1qgl6hw_1280-gray.jpg", "p7pnqdW5h11qgl6hw_1280-gray.jpg", "p7po2cAiei1qgl6hw_1280-gray.jpg", "p7po4vT9u81qgl6hw_1280-gray.jpg", "p7po7tEZEW1qgl6hw_1280-gray.jpg", "p7po8tl2PH1qgl6hw_1280-gray.jpg", "p7qmwsC2Mc1qgl6hw_1280-gray.jpg", "p7qnn0Vbn21qgl6hw_1280-gray.jpg", "p7qnqe8wAh1qgl6hw_1280-gray.jpg", "p7qnx4YDEc1qgl6hw_1280-gray.jpg", "p7qoe1pKXz1qgl6hw_1280-gray.jpg", "p7qoqm6wGz1qgl6hw_1280-gray.jpg", "p7qosqD1ky1qgl6hw_1280-gray.jpg", "p7qoyzHgNf1qgl6hw_1280-gray.jpg", "p7qp5nxviA1qgl6hw_1280-gray.jpg", "p7qp9qpRF71qgl6hw_1280-gray.jpg", "p7qpba31Tz1qgl6hw_1280-gray.jpg", "p7qpeyd43Q1qgl6hw_1280-gray.jpg", "p7qpi7aW1B1qgl6hw_1280-gray.jpg", "p7snz6DSI51qgl6hw_1280-gray.jpg", "p7so0ccROw1qgl6hw_1280-gray.jpg", "p7so9m0RZr1qgl6hw_1280-gray.jpg", "p7so804giF1qgl6hw_1280-gray.jpg", "p7soehjVIn1qgl6hw_1280-gray.jpg", "p7sog9ZV5d1qgl6hw_1280-gray.jpg", "p7ygudCF6B1qgl6hw_1280-gray.jpg", "p7ygw798Gt1qgl6hw_1280-gray.jpg", "p7ygz2zGVz1qgl6hw_1280-gray.jpg", "p7yh09xFJP1qgl6hw_1280-gray.jpg", "p7ztispWRy1qgl6hw_1280-gray.jpg"];
    var i = 0;
    var pid = 0;
    ps.forEach(p => {
      p.id = "p-" + pid;
      pid++;
      if (Math.random() > .35 && images.length) {
        var img = document.createElement('img');
        var src = images.shift();
        img.src = "img/" + src;
        img.id = 'image-' + i;
        let dice = randomIntFromInterval(1, 3)
        if (dice === 1) {
          img.className = "imgFullPage";
          p.parentNode.insertBefore(img, p.nextSibling);

        } else if (dice === 2) {
          img.className = "imgFullSpread";
          p.parentNode.insertBefore(img, p.nextSibling);

        } else {
          img.className = "noluck";
          p.appendChild(img);

        }
        i++;
      }
    });

    // attribue des id à chaque span du texte, afin de pouvoir leur donner du style
    // voir css.py et vars.css qui génère la variation de graisse et chasse pour la fonte
    // des tailles et positions aléatoires pour les images
    var spans = content.querySelectorAll('.text-section span');
    var spanid = 0;
    spans.forEach(span => {
      span.id = 'span-' + spanid;
      spanid++;
    })
  }

}

Paged.registerHandlers(revolution);

function randomIntFromInterval(min, max) { // min and max included 
  return Math.floor(Math.random() * (max - min + 1) + min)
}
