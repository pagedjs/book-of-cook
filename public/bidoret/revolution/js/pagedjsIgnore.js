// let’s try to hide some css from pagedjs:
// if your css start with /* pagedjs-ignore  */, 
// then the whole css is ignored

// not in use

class postCSS extends Paged.Handler {

    // everytime pagedjs open a new style sheet
    beforeTreeParse(text, sheet) {
        // check if the text of the stylesheet contains pagedjs-ignore (anywhere in teh file)
    if  (text.includes('pagedjs-ignore')) {
            // reuse the text of the stylesheet in a style element in the head
            let ignored = document.createElement('style');
            ignored.innerText = text;
            ignored.dataset.pagedjsInsertedStyles = true;
            document.head.appendChild(ignored);
        }
    }
     
}

Paged.registerHandlers(postCSS);
