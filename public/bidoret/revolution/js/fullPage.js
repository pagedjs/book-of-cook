// lets you manualy add classes to some pages elements
// to simulate page floats.
// works only for elements that are not across two pages

const classElemFullPage = "imgFullPage"; // ← class of full page images
const classElemFullSpread = "imgFullSpread"; // ← class of full page images

class fullPage extends Paged.Handler {
  constructor(chunker, polisher, caller) {
    super(chunker, polisher, caller);
    this.floatFullPage = [];
    this.floatFullSpread = [];
    this.pageFilled = false;
    this.floatFullElements = new Set;
    this.tempSpread = new Set;
    this.waitingSpread = new Set;
    this.cloneset = false;
    this.clonestyle = `<style data-pagedjs-inserted-styles="true">.cloned {
    position: absolute;
    inset: 0;
    margin: 0;
    padding: 0;
    height: 100%;
    width: 100%;
    object-fit: cover;
}</style>`;
    this.pauseToken = undefined;
    this.resetBreakTokenCounter = 0;
  }



  // this script will run on every css declaration 

  onDeclaration(declaration, dItem, dList, rule) {
    // if the declaration contains --page-float and taht page 
    if (declaration.property == "--page-float") {
      // if the value of that declaration is 'full-page'
      if (declaration.value.value.includes("full-page")) {
        // we use css tree to generate the list of selector
        let sel = csstree.generate(rule.ruleNode.prelude);
        // we make sure that any data-id is replaced by an id to be reused by pagedjs
        sel = sel.replace('[data-id="', "#");
        sel = sel.replace('"]', "");
        // we add the selector to the list and keep that in a variable shared by all the hooks.
        sel.split(',').forEach(el => this.floatFullPage.push(el));
      }
      if (declaration.value.value.includes("spread")) {
        // we use css tree to generate the list of selector
        let sel = csstree.generate(rule.ruleNode.prelude);
        // we make sure that any data-id is replaced by an id to be reused by pagedjs
        sel = sel.replace('[data-id="', "#");
        sel = sel.replace('"]', "");
        // we add the selector to the list and keep that in a variable shared by all the hooks.
        sel.split(',').forEach(el => this.floatFullSpread.push(el));
      }
      // here we add the clonestyle css to the document (but we check first that it’s hasn’t been done yet to add it only once)
      if (this.cloneset == false) {
        document.head.insertAdjacentHTML('beforeend', this.clonestyle);
        this.cloneset = true;
      }

    }
  }

  afterParsed(content) {
    // when all the content is parsed, check for all the selectors in this.floatFullPage, and add a class (with the classElemFullPage variable set in the top of the file)
    if (this.floatFullPage) {
      this.floatFullPage.forEach((elNBlist) => {
        content.querySelectorAll(elNBlist).forEach((el) => {
          el.classList.add(classElemFullPage);
        });
      });
    }
    if (this.floatFullSpread) {
      this.floatFullSpread.forEach((elNBlist) => {
        content.querySelectorAll(elNBlist).forEach((el) => {
          el.classList.add(classElemFullSpread);
        });
      });
    }
  }


  // here comes the magic.
  // renderNode runs everytime pagedjs want to render an element
  // When we render the node (when the element will be added to the page)

  renderNode(node, sourcenode) {
    // if the node is of type element
    if (node.nodeType == 1) {
      // if the node has the class  
      if (node.classList.contains(classElemFullSpread)) {
        // clone the node
        let clone = node.cloneNode(true);
        // remove the id of the clone (to be valid html)
        clone.id = '';
        // add the cloned class to the clone
        clone.classList.add("cloned");

        // add the clone to the list of the element that will be push on their own page
        this.tempSpread.add(clone);

        // add a border to the element to be cloned (to debug)
        node.style.border = '1px solid magenta';
        // hide it. We don’t remove it from the dom, otherwise pagedjs will be lost when checking the source and the note
        node.style.display = "none";
        // hide the sourcenode, we don’t need it anymore after all. (this is may be problematic one day, but i’ll be far when that happen )
        // node.style.display = "none";
        this.resetBreakTokenCounter = this.resetBreakTokenCounter + 2;
      }
      if (node.classList.contains(classElemFullPage)) {
        // clone the node
        let clone = node.cloneNode(true);
        // remove the id of the clone (to be valid html)
        clone.id = '';
        // add the cloned class to the clone
        clone.classList.add("cloned");

        // add the clone to the list of the element that will be push on their own page
        this.floatFullElements.add(clone);

        // add a border to the element to be cloned (to debug)
        node.style.border = '1px solid magenta';
        // hide it. We don’t remove it from the dom, otherwise pagedjs will be lost when checking the source and the note
        node.style.display = "none";
        // hide the sourcenode, we don’t need it anymore after all. (this is may be problematic one day, but i’ll be far when that happen )
        sourcenode.style.display = "none";
        this.resetBreakTokenCounter++;
      }
    }
  }

  // this will happen before the layout of each and every page

  beforePageLayout(page) {
    this.pageFilled = false;




    // if spread
    // if there is an element in the tempSpread
    if (this.tempSpread.size >= 1 && page.element.classList.contains("pagedjs_left_page")) {
      // add the pullpage class to the page
      page.element.classList.add('fullpaged', 'spread-left')

      // insert a clone of the first element of the set (the ... spread help us find the first element)
      page.element.querySelector('.pagedjs_sheet').insertAdjacentElement('afterbegin', [...this.tempSpread][0].cloneNode(true));

      // move to the waiting spread to simplify right page
      this.waitingSpread.add([...this.tempSpread][0]);

      this.tempSpread.delete([...this.tempSpread][0]);

      // say the page is filled. so you stop adding stuff
      this.pageFilled = true;

    }

    // assuming that this will always be a right page
    if (this.waitingSpread.size >= 1 && this.pageFilled == false) {
      page.element.classList.add('fullpaged', 'spread-right')

      page.element.querySelector('.pagedjs_sheet').insertAdjacentElement('afterbegin', [...this.waitingSpread][0]);

      //then delete the element from the spread list

      this.waitingSpread.delete([...this.waitingSpread][0]);

      this.pageFilled = true;
    }


    if (this.pageFilled == false && this.floatFullElements.size >= 1) {
      // add the pullpage class to the page
      page.element.classList.add('fullpaged')
      // insert the first element of the set (the ... spread help us find the first element)
      page.element.querySelector('.pagedjs_sheet').insertAdjacentElement('afterbegin', [...this.floatFullElements][0]);
      //then delete the element from the this.floatFullElements][0]
      this.floatFullElements.delete([...this.floatFullElements][0]);

      this.pageFilled = true;

    }


  }


  // this will happen after every page is finished (yeah!)

  afterPageLayout(pageElement, page, breakToken) {
      if (this.pageFilled = true) {
        // count the breakTokens!
      this.resetBreakTokenCounter = this.resetBreakTokenCounter - 1;

      }

    // if the page class contain full page
    if (pageElement.classList.contains('fullpaged')) {
      // hide everything in the margin
      pageElement.querySelectorAll(".pagedjs_margin-content")
        .forEach((marginContent) => {
          marginContent.remove()
        });
      // empty the page with image full page
      pageElement.querySelector(".pagedjs_page_content").remove();
    }

  }

  onBreakToken(breakToken, overflow, rendered) {
    
    console.log(`breakToken`, breakToken)
    console.log(`this.pauseToken`, this.pauseToken)
    console.log(`this.resetBreakTokenCounter`, this.resetBreakTokenCounter)
    // check the pause token and the breaktoken counter to see if all page are set
    if (this.pauseToken != undefined && this.resetBreakTokenCounter == 0) {
      breakToken.offset = this.pauseToken.offset + this.resetBreakTokenCounter;
      breakToken.node = this.pauseToken.node;
      this.pauseToken = undefined;

      // console.log(breakToken);
    } else if (this.pauseToken == undefined && this.resetBreakTokenCounter > 0) {
      this.pauseToken = { ...breakToken };
    }

  }

}




Paged.registerHandlers(fullPage);
